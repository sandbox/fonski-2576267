<?php

use Drupal\maps_import\Profile\Profile;

/**
 * Form builder.
 */
function maps_auto_config_form($form, &$form_state, Profile $profile) {
  $form = array();

  if (is_null($profile)) {
    $profile = $form_state['multistep_values']['profile'];
  }
  else {
    $form_state['multistep_values']['profile'] = $profile;
  }

  $stage = isset($form_state['stage']) ? $form_state['stage'] : 'medias';
  switch ($stage) {
    case 'medias':
      $form = maps_auto_config_medias_form($form, $form_state, $profile);
      break;

    case 'natures':
      $form = maps_auto_config_natures_form($form, $form_state, $profile);
      break;

    case 'attributes':
      $entity_types = $form_state['multistep_values']['natures'];
      $nature_ids = array_keys($form_state['multistep_values']['natures']);

      $form = maps_auto_config_attributes_form($form, $form_state, $profile, reset($entity_types), reset($nature_ids));
      break;

    case 'operations':
      $form = maps_auto_config_operations_form($form, $form_state, $profile);
      break;
  }

  return $form;
}

/**
 * Form builder.
 */
function maps_auto_config_form_submit($form, &$form_state) {
  $profile = $form_state['multistep_values']['profile'];

  $stage = isset($form_state['stage']) ? $form_state['stage'] : 'medias';
  switch ($stage) {
    case 'medias':
      maps_auto_config_medias_form_submit($form, $form_state, $profile);
      break;

    case 'natures':
      maps_auto_config_natures_form_submit($form, $form_state, $profile);
      break;

    case 'attributes':
      $entity_types = $form_state['multistep_values']['natures'];
      $nature_ids = array_keys($form_state['multistep_values']['natures']);

      maps_auto_config_attributes_form_submit($form, $form_state, $profile, reset($nature_ids), reset($entity_types));
      break;

    case 'operations':
      maps_auto_config_operations_form_submit($form, $form_state, $profile);
      break;
  }

  $form_state['rebuild'] = TRUE;
}

/**
 *
 */
function maps_auto_config_medias_form($form, &$form_state, Profile $profile) {
  $form = array();

  // Retrieve the media types from the configuration.
  $media_types = $profile->getConfigurationTypes('media_type', $profile->getDefaultLanguage());

  $form['#tree'] = TRUE;

  $form['markup'] = array(
    '#markup' => t('Select the media types to import'),
  );

  $form['media_types'] = array();
  foreach ($media_types as $media_type) {
    $form['media_types'][$media_type['id']] = array(
      '#type' => 'checkbox',
      '#title' => $media_type['title'],
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 *
 */
function maps_auto_config_medias_form_submit($form, &$form_state) {
  $profile = $form_state['multistep_values']['profile'];
  $media_types = $profile->getConfigurationTypes('media_type', $profile->getDefaultLanguage());


  $form_state['multistep_values']['media_types'] = array();
  foreach ($form_state['values']['media_types'] as $media_type_id => $value) {
    if (!$value) {
      continue;
    }

    // Create a new media converter.
    if (!$converter = maps_auto_config_load_converter($profile, strtolower($media_types[$media_type_id]['title']))) {
      $converter = maps_auto_config_create_media_converter_and_filters($profile, $media_type_id);
    }

    $form_state['multistep_values']['media_types'][$media_type_id] = $converter;
  }

  $form_state['stage'] = 'natures';
}

/**
 *
 */
function maps_auto_config_natures_form($form, &$form_state, Profile $profile) {
  // Retrieve all object natures from MaPS configuration.
  $natures = $profile->getConfigurationTypes('object_nature', $profile->getLanguage());

  // Retrieve all defined entity types.
  $options = array('' => t('- None -'));
  $entities_info = entity_get_info();

  // Since we can not create bundle generically, we limit the available entity types.
  $entity_types = array(
    'node' => $entities_info['node'],
    //'taxonomy_term' => $entities_info['taxonomy_term'],
  );

  foreach ($entity_types as $entity_type_name => $entity_type) {
    $options[$entity_type_name] = $entity_type['label'];
  }

  $form['#tree'] = TRUE;
  // Create the select for each natures.
  foreach ($natures as $nature) {
    $form['natures'][$nature['id']] = array(
      '#title' => $nature['title'],
      '#type' => 'select',
      '#options' => $options,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 *
 */
function maps_auto_config_natures_form_submit($form, &$form_state) {
  $form_state['multistep_values']['natures'] = array();
  $form_state['stage'] = 'attributes';

  $profile = $form_state['multistep_values']['profile'];
  $message = array();

  if (empty($form_state['multistep_values']['converter_by_nature'])) {
    $form_state['multistep_values']['converter_by_nature'] = array();
  }

  $entity_types = $form_state['values']['natures'];
  $natures = $profile->getConfigurationTypes('object_nature', $profile->getLanguage());

  // Loop through entity types.
  foreach ($entity_types as $nature_id => $entity_type) {
    // Skip if not chosen.
    if (empty($entity_type)) {
      continue;
    }
    $form_state['multistep_values']['natures'][$nature_id] = $entity_type;

    $nature = $natures[$nature_id];

    // Create the new bundle.
    $object = new StdClass();

    // Create the new entity type.
    switch ($entity_type) {
      // Create new content type.
      case 'node':
        $object->type = strtolower($nature['code']);
        $object->name = $nature['title'];
        $object->base = 'node_content';
        $object->module = 'maps_auto_config';
        $object->locked = FALSE;

        if (!node_type_load(strtolower($nature['code']))) {
          node_type_save($object);
          $message[] = t('%title node type created.', array('%title' => $nature['title']));
        }

        break;
    }

    // Create the converter and the filter if they don't exist.
    if (!$converter = maps_auto_config_load_converter($profile, strtolower($nature['code']))) {
      // Create the converter and the related filters.
      $converter = maps_auto_config_create_object_converter_and_filters($profile, $nature, $entity_type);

      $message[] = t('Converters and filters created for content type %title', array('%title' => $nature['title']));
    }

    // Add the object/media mapping items.
    foreach ($form_state['multistep_values']['media_types'] as $media_type_id => $media_converter) {
      // @todo Duplicate code !!!!
      $field_name = maps_auto_config_generate_field_name($media_converter->getBundle() . 's');
      $field_info = field_info_field($field_name);

      // Create the field.
      if ($field_info == '') {
        $type = $media_converter->getBundle();

        maps_auto_config_create_field($field_name, $type, array('multiple' => 1));
      }

      // Create the field instance.
      $field_exists = field_info_instance($converter->getEntityType(), $field_name, $converter->getBundle());
      if (!$field_exists) {
        maps_auto_config_create_instance($field_name, $media_converter->getTitle(), $converter->getEntityType(), $converter->getBundle());
      }

      if (!maps_auto_config_mapping_item_exists($converter, 'media:' .$media_type_id , $field_name)) {
        $data = array(
          'media_type' => '1',
          'media_start_range' => '1',
          'media_limit_range' => '50',
        );
        maps_auto_config_create_mapping_item($converter, 'media:' . $media_type_id, $field_name, $data);
      }
    }

    $form_state['multistep_values']['converter_by_nature'][$nature['id']] = $converter;
  }

  if (!empty($message)) {
    drupal_set_message(theme('item_list', array('items' => $message)));
  }
}

/**
 *
 */
function maps_auto_config_attributes_form($form, &$form_state, Profile $profile, $entity_type, $nature_id) {
  $form = array();

  $natures = $profile->getConfigurationTypes('object_nature', $profile->getLanguage());
  $nature = $natures[$nature_id];

  drupal_set_title(t('Attributes for nature ') . $nature['title']);

  $form['#entity_type'] = $entity_type;
  $form['#nature_id'] = $nature_id;

  $attributes = $profile->getConfigurationTypes('attribute', $profile->getLanguage());
  $attribute_sets = $profile->getConfigurationTypes('attribute_set', $profile->getLanguage());
  $attribute_sets_has_attributes = $profile->getConfigurationTypes('attribute_set_has_attribute');

  $form['#tree'] = TRUE;
  foreach ($attribute_sets as $attribute_set_id => $attribute_set) {
    $form['attribute_set'][$attribute_set_id] = array(
      '#title' => $attribute_set['title'],
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
    );

    // Retrieve the attributes for the current attribute set.
    $attribute_set_has_attribute = $attribute_sets_has_attributes[$attribute_set_id];
    $data = unserialize($attribute_set_has_attribute['data']);

    foreach($data as $attribute_id) {
      $attribute = $attributes[$attribute_id];
      $form['attribute_set'][$attribute_set_id][$attribute_id] = array(
        '#title' => $attribute['title'],
        '#type' => 'checkbox',
      );
    }
  }

  // Remove the current element.
  unset($form_state['multistep_values']['natures'][$nature_id]);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 *
 */
function maps_auto_config_attributes_form_submit($form, &$form_state) {
  $profile = $form_state['multistep_values']['profile'];

  $attributes = $profile->getConfigurationTypes('attribute', $profile->getLanguage());

  $values = $form_state['values']['attribute_set'];

  $converter = $form_state['multistep_values']['converter_by_nature'][$form['#nature_id']];

  foreach ($values as $attribute_set) {
    foreach ($attribute_set as $attribute_id => $value) {
      if ($value == 0) {
        continue;
      }

      $attribute = $attributes[$attribute_id];

      $field_name = maps_auto_config_generate_field_name($attribute['code']);
      $field_info = field_info_field($field_name);

      // Create the field.
      if ($field_info == '') {
        $data = unserialize($attribute['data']);
        $type = $data['attribute_type_code'];

        maps_auto_config_create_field($field_name, $type, $data);
      }

      // Create the field instance.
      $field_exists = field_info_instance($converter->getEntityType(), $field_name, $converter->getBundle());
      if (!$field_exists) {
        maps_auto_config_create_instance($field_name, $attribute['title'], $converter->getEntityType(), $converter->getBundle());
      }

      // Create the mapping element.
      if (!maps_auto_config_mapping_item_exists($converter, 'attribute:' . $attribute_id, $field_name)) {
        maps_auto_config_create_mapping_item($converter, 'attribute:' . $attribute_id, $field_name);
      }
    }
  }

  maps_auto_config_add_label_mapping_item($converter);

  if (empty($form_state['multistep_values']['natures'])) {
    $form_state['stage'] = 'operations';
  }
}

/**
 *
 */
function maps_auto_config_operations_form($form, &$form_state, Profile $profile) {
  $form = array();

  $form['process'] = array(
    '#title' => t('Process a full import'),
    '#description' => t('Process a full import and mapping for this profile'),
    '#type' => 'checkbox',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Finish'),
  );

  $form['#validate'] = 'maps_auto_config_operations_form_validate';

  return $form;
}

/**
 *
 */
function maps_auto_config_operations_form_validate($form, $form_state) {
  if (variable_get('maps_import_lock')) {
    form_set_error('', t('There is already operations under processing'));
  }
}

/**
 *
 */
function maps_auto_config_operations_form_submit($form, $form_state, Profile $profile) {
  $operations = maps_import_get_import_operations($profile);

  $batch_operations = array();
  foreach ($operations as $operation) {
    $op = new $operation['class']($profile);
    $batch_operations = array_merge($batch_operations, $op->batchOperations());
  }

  $batch = array(
    'operations' => $batch_operations,
    'title' => t('Update for the profile %title', array('%title' => $profile->getTitle())),
    'file' => drupal_get_path('module', 'maps_import') . '/admin/maps_import.admin.inc',
    'finished' => 'maps_import_import_finished',
  );

  variable_set('maps_import_lock', 1);
  batch_set($batch);
}
